LABEL description="Debian image with rust-coreutils"

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

ADD debian-rust-coreutils.tar.gz /
