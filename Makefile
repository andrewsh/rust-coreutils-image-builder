IMAGENAME=debian-rust-coreutils

build-docker: build-rootfs Dockerfile
	docker build . -t $(IMAGENAME)

build-rootfs: $(IMAGENAME).tar.gz
	@[ -f $< ] || ( echo $< not found ; exit 1 )

%.tar.gz %.img.gz %.img.bmap: %.yaml
	debos $<

.PHONY: build-docker build-rootfs
